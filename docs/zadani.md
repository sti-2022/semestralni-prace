# chatovací aplikace (jednoduché okno jako na různých webech) - něco jako chatbot
- testy - 80% coverage
- CI/CD
- podpora více klientů
- neudržovat komunikaci/historii
- main a feature větve
- zabezpečení proti SPAMu apod.

## 2 části:
- uživatelská (klient) - Frontend
  - jednoduché
  - podpora značkovacích jazyků - (HTML, MD)
    - umí zobrazit nějaká data např. v tabulce
  - odesílání dotazů
  - podpora dlouho trvajících dotazů - zobrazí se indikátor

- server - Backend
  - odpovídá na dotazy z frontendu
    - dotazy v češtině nebo angličtině
    - analýza zadaného textu a reakce dle šablon
    - zpracovat jeden dotaz nebo více najednou, zmínit ale v DSP
    - dotazy:
      - jaký je čas (kolik je hodin) => pošle aktuální čas serveru s časovou zónou
      - jak se jmenuješ => název aplikace/chatbota
      - kurz eura => aktuální datum a kurz, dnešní a případně o víkendu zašle z pátku
      - historie eura => zobrazí veškeré hodnoty eura od spuštění verze
      - help/nápověda => zobrazí dotazy
      - další možné do budoucna:
        - kurz eura z X => kurz ze zadaného data
        - kurz X => jiný kurz
  - podpora zpracování formátovaných dat - obrázky, tabulky apod.
  - zabezpečení API
    - ne jméno a heslo!
    - omezení IP?
    - unikátně vygenerovaný hash?

## deadlines
- do 2 týdn dnů od 31.3.
  - DSP

- 4-5 týdnů do konce semestru
  - první verze aplikace

## rizika:
- sanitizace vstupu
- formát odpovědi z API na měny
- zabezpečení proti vnějšímu světu
- výpadky síťové komunikace
