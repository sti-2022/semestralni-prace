package cz.woi.sti.backend.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "cz.woi.sti.backend")
@Data
public class InformationConfiguration {
    private String applicationName;
    private String timezone;
}
