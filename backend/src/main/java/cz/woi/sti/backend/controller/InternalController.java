package cz.woi.sti.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.woi.sti.backend.service.ExchangeDataDownloadService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class InternalController {
    private ExchangeDataDownloadService exchangeDataDownloadService;
    private ObjectMapper mapper;

    @GetMapping("/download-data")
    public ResponseEntity<ObjectNode> startDataDownload() {
        ObjectNode resBody = mapper.createObjectNode();
        ResponseEntity.BodyBuilder res;
        if (exchangeDataDownloadService.downloadSupportedCurrenciesData()) {
            resBody.put("status", 200);
            resBody.put("message", "ok");
            res = ResponseEntity.ok();
        } else {
            resBody.put("status", 503);
            resBody.put("message", "data download failed");
            res = ResponseEntity.status(503);
        }
        return res.body(resBody);
    }
}
