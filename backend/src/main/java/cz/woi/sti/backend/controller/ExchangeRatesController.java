package cz.woi.sti.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.woi.sti.backend.model.ExchangeRate;
import cz.woi.sti.backend.service.ExchangeRatesService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ExchangeRatesController {
    private ObjectMapper mapper;
    private ExchangeRatesService exchangeRatesService;

    @GetMapping(value = "/exchange-rate")
    public ResponseEntity<ObjectNode> exchangeRate() {
        return exchangeRate("EUR");
    }

    @GetMapping(value = "/exchange-rate/{currency}")
    public ResponseEntity<ObjectNode> exchangeRate(@PathVariable String currency) {
        if (exchangeRatesService.isCurrencyUnsupported(currency)) {
            return getUnsupportedCurrencyResponse();
        }

        ObjectNode resBody = mapper.createObjectNode();
        ResponseEntity.BodyBuilder res;
        Double exchangeRate = exchangeRatesService.getExchangeRate(currency);
        resBody.put("currency", currency);
        resBody.put("rate", exchangeRate);
        res = ResponseEntity.ok();
        return res.body(resBody);
    }

    @GetMapping(value = "/rate-history")
    public ResponseEntity<ObjectNode> rateHistory() {
        return rateHistory("EUR");
    }

    @GetMapping(value = "/rate-history/{currency}")
    public ResponseEntity<ObjectNode> rateHistory(@PathVariable String currency) {
        if (exchangeRatesService.isCurrencyUnsupported(currency)) {
            return getUnsupportedCurrencyResponse();
        }

        ObjectNode resBody = mapper.createObjectNode();
        ResponseEntity.BodyBuilder res;
        resBody.put("currency", currency);
        ArrayNode ratesNode = resBody.putArray("rates");
        for (ExchangeRate exchangeRate : exchangeRatesService.getRateHistory(currency)) {
            ObjectNode rateNode = mapper.createObjectNode();
            rateNode.put("date", exchangeRate.getDate().toString());
            rateNode.put("rate", exchangeRate.getRate());
            ratesNode.add(rateNode);
        }
        res = ResponseEntity.ok();
        return res.body(resBody);
    }

    private ResponseEntity<ObjectNode> getUnsupportedCurrencyResponse() {
        ObjectNode resBody = mapper.createObjectNode();
        ResponseEntity.BodyBuilder res;
        resBody.put("status", 400);
        resBody.put("message", "currency unsupported");
        res = ResponseEntity.badRequest();
        return res.body(resBody);
    }
}
