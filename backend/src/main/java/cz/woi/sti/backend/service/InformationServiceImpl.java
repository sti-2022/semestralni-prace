package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.InformationConfiguration;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Service
@AllArgsConstructor
public class InformationServiceImpl implements InformationService {
    private InformationConfiguration informationConfiguration;

    @Override
    public String getApplicationName() {
        return informationConfiguration.getApplicationName();
    }

    @Override
    public String getCurrentTimezoneTime() {
        return ZonedDateTime.now(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(informationConfiguration.getTimezone())).format(DateTimeFormatter.ofPattern("HH:mm:ss VV"));
    }
}
