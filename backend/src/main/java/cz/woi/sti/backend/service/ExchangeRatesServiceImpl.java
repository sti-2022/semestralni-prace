package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.ExchangeRatesConfiguration;
import cz.woi.sti.backend.model.ExchangeRate;
import cz.woi.sti.backend.repository.ExchangeRateRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class ExchangeRatesServiceImpl implements ExchangeRatesService {
    ExchangeRateRepository exchangeRateRepository;

    @Override
    public double getExchangeRate(String currency) {
        if (isCurrencyUnsupported(currency)) {
            throw new IllegalArgumentException("currency not supported");
        }
        return exchangeRateRepository.findByDateAndCurrency(LocalDate.now(), currency).getRate();
    }

    @Override
    public List<ExchangeRate> getRateHistory(String currency) {
        if (isCurrencyUnsupported(currency)) {
            throw new IllegalArgumentException("currency not supported");
        }
        return exchangeRateRepository.findAllByCurrency(currency);
    }

    @Override
    public boolean isCurrencyUnsupported(String currency) {
        return !ExchangeRatesConfiguration.SUPPORTED_CURRENCIES.contains(currency);
    }
}
