package cz.woi.sti.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.woi.sti.backend.service.CommandService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class CommandController {
    private ObjectMapper mapper;
    private CommandService commandService;

    @PostMapping("/command")
    public ResponseEntity<ObjectNode> analyzeCommand(@RequestBody ObjectNode reqBody) {
        ObjectNode resBody = mapper.createObjectNode();
        if (Objects.isNull(reqBody.get("commandText"))) {
            resBody.put("status", 400);
            resBody.put("message", "data malformed, missing 'commandText'");
            return ResponseEntity.badRequest().body(resBody);
        }

        Pair<String, String> command = commandService.getMostProbableCommand(reqBody.get("commandText").asText());
        if (command == null) {
            resBody.put("status", 422);
            resBody.put("message", "command not known");
            return ResponseEntity.unprocessableEntity().body(resBody);
        }

        resBody.put("command", command.getLeft());
        resBody.put("endpoint", command.getRight());
        return ResponseEntity.ok(resBody);
    }

    @GetMapping("/help")
    public ResponseEntity<ArrayNode> getHelp() {
        ArrayNode resBody = mapper.createArrayNode();
        for (Triple<String, String, String> command : commandService.getSupportedCommandsHelp()) {
            ObjectNode commandNode = mapper.createObjectNode();
            commandNode.put("command", command.getLeft());
            commandNode.put("help", command.getMiddle());
            resBody.add(commandNode);
        }
        return ResponseEntity.ok(resBody);
    }
}
