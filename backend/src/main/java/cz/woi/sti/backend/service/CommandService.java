package cz.woi.sti.backend.service;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.List;

public interface CommandService {
    Pair<String, String> getMostProbableCommand(String commandText);

    List<Triple<String, String, String>> getSupportedCommandsHelp();
}
