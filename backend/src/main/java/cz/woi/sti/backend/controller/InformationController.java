package cz.woi.sti.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.woi.sti.backend.service.InformationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class InformationController {
    private ObjectMapper mapper;
    private InformationService informationService;

    @GetMapping("/name")
    public ObjectNode name() {
        ObjectNode res = mapper.createObjectNode();
        res.put("name", informationService.getApplicationName());
        return res;
    }

    @GetMapping("/time")
    public ObjectNode time() {
        ObjectNode res = mapper.createObjectNode();
        res.put("time", informationService.getCurrentTimezoneTime());
        return res;
    }
}
