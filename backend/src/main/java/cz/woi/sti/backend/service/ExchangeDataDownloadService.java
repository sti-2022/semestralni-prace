package cz.woi.sti.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface ExchangeDataDownloadService {
    void downloadCurrencyData(String currency) throws JsonProcessingException;

    boolean downloadSupportedCurrenciesData();
}
