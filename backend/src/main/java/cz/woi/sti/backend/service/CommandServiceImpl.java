package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.CommandConfiguration;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CommandServiceImpl implements CommandService {
    @Override
    public Pair<String, String> getMostProbableCommand(String commandText) {
        String commandTextProcessed = commandText
                .toLowerCase()
                .replaceAll("[.!?,]", " ")
                .replaceAll("\\s+", " ")
                .strip();

        for (Triple<String, String, String> supportedCommand : CommandConfiguration.SUPPORTED_COMMANDS) {
            if (commandTextProcessed.contains(supportedCommand.getLeft())) {
                return Pair.of(supportedCommand.getLeft(), supportedCommand.getRight());
            }
        }
        return null;
    }

    @Override
    public List<Triple<String, String, String>> getSupportedCommandsHelp() {
        return CommandConfiguration.SUPPORTED_COMMANDS;
    }
}
