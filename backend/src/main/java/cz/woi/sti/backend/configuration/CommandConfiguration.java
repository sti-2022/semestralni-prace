package cz.woi.sti.backend.configuration;

import lombok.Data;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@Data
public class CommandConfiguration {
    public static final List<Triple<String, String, String>> SUPPORTED_COMMANDS = List.of(
            Triple.of("help", "this message", "help"),
            Triple.of("time", "current server time", "time"),
            Triple.of("name", "name of application", "name"),
            Triple.of("history of euro exchange rate", "history of euro since the start of application", "exchange-rate"),
            Triple.of("euro exchange rate", "current euro exchange rate", "rate-history")
    );
}
