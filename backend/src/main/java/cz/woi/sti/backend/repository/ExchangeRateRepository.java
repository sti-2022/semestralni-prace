package cz.woi.sti.backend.repository;

import cz.woi.sti.backend.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {
    List<ExchangeRate> findAllByCurrency(String currency);

    ExchangeRate findByDateAndCurrency(LocalDate date, String currency);
}
