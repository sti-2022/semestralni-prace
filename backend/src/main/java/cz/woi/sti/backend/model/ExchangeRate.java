package cz.woi.sti.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDate;

@Entity
@Table(name = "exchange_rates", uniqueConstraints = {@UniqueConstraint(columnNames = {"date", "currency"})})
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ExchangeRate extends BaseEntity {
    @Column(name = "date")
    private LocalDate date;

    @Column(name = "currency")
    private String currency;

    @Column(name = "rate")
    private Double rate;

    public ExchangeRate(String currency, Double rate) {
        this.date = LocalDate.now();
        this.currency = currency;
        this.rate = rate;
    }
}
