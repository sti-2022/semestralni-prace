package cz.woi.sti.backend.service;

import cz.woi.sti.backend.model.ExchangeRate;

import java.util.List;

public interface ExchangeRatesService {
    double getExchangeRate(String currency);

    List<ExchangeRate> getRateHistory(String currency);

    boolean isCurrencyUnsupported(String currency);
}
