package cz.woi.sti.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.woi.sti.backend.configuration.ExchangeRatesConfiguration;
import cz.woi.sti.backend.model.ExchangeRate;
import cz.woi.sti.backend.repository.ExchangeRateRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;

@Service
@AllArgsConstructor
@Log4j2
public class ExchangeDataDownloadServiceImpl implements ExchangeDataDownloadService {
    private ExchangeRatesConfiguration exchangeRatesConfiguration;
    private RestTemplateBuilder restTemplateBuilder;
    private ExchangeRateRepository exchangeRateRepository;
    private ObjectMapper mapper;

    @Override
    public void downloadCurrencyData(String currency) throws JsonProcessingException {
        log.info("started data download for curency {}", currency);
        RestTemplate req = restTemplateBuilder.build();
        String assembledURI = exchangeRatesConfiguration.getUrl() +
                "/" + exchangeRatesConfiguration.getKey() +
                "/" + "pair" +
                "/" + currency +
                "/" + "CZK";
        ResponseEntity<String> res = req.getForEntity(URI.create(assembledURI), String.class);
        if (res.getStatusCode().is4xxClientError() || res.getStatusCode().is5xxServerError()) {
            log.error("data download failed for curency {}", currency);
            throw new RuntimeException("data download for " + currency + " failed");
        }
        ObjectNode resJson = (ObjectNode) mapper.readTree(res.getBody());
        if (Objects.isNull(exchangeRateRepository.findByDateAndCurrency(LocalDate.now(), currency))) {
            exchangeRateRepository.save(new ExchangeRate(
                    resJson.get("base_code").asText(),
                    resJson.get("conversion_rate").asDouble())
            );
        }
        log.info("data download successful for curency {}", currency);
    }

    @Override
    public boolean downloadSupportedCurrenciesData() {
        try {
            for (String SUPPORTED_CURRENCY : ExchangeRatesConfiguration.SUPPORTED_CURRENCIES) {
                downloadCurrencyData(SUPPORTED_CURRENCY);
            }
        } catch (JsonProcessingException e) {
            return false;
        }
        return true;
    }

    @Scheduled(cron = "0 0 3 * * *")
    private void scheduledDownload() {
        downloadSupportedCurrenciesData();
    }
}
