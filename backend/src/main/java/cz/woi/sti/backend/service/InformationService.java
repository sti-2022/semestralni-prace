package cz.woi.sti.backend.service;

public interface InformationService {
    String getApplicationName();

    String getCurrentTimezoneTime();
}
