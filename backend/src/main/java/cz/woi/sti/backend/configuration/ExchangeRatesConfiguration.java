package cz.woi.sti.backend.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "cz.woi.sti.exchange-rates.api")
@Data
public class ExchangeRatesConfiguration {
    private String url;
    private String key;
    public static final List<String> SUPPORTED_CURRENCIES = List.of(
            "EUR",
            "USD",
            "GBP"
    );
}
