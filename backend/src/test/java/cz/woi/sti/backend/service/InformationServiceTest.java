package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.InformationConfiguration;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class InformationServiceTest {

    @Mock
    InformationConfiguration informationConfiguration;

    @InjectMocks
    InformationServiceImpl informationService;

    @BeforeEach
    void setUp() {
        lenient().when(informationConfiguration.getApplicationName()).thenReturn("test");
        lenient().when(informationConfiguration.getTimezone()).thenReturn("UTC");
    }

    @Test
    void getApplicationName() {
        String applicationName = informationService.getApplicationName();
        Assertions.assertThat(applicationName).isEqualTo("test");
    }

    @Test
    void getCurrentTimezoneTime() {
        Assertions.assertThat(informationService.getCurrentTimezoneTime()).isEqualTo(
                ZonedDateTime.now(ZoneId.of("UTC"))
                        .withZoneSameInstant(ZoneId.of(informationConfiguration.getTimezone()))
                        .format(DateTimeFormatter.ofPattern("HH:mm:ss VV")));
    }
}