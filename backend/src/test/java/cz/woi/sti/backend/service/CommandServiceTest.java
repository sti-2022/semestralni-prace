package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.CommandConfiguration;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CommandServiceTest {

    @InjectMocks
    CommandServiceImpl commandService;

    @Test
    void getMostProbableCommand_withSupportedCommands() {
        CommandConfiguration.SUPPORTED_COMMANDS.forEach(
                supportedCommand -> Assertions.assertThat(
                        commandService.getMostProbableCommand(supportedCommand.getLeft())).isNotNull());
    }

    @Test
    void getMostProbableCommand_withUnsupportedCommands() {
        Assertions.assertThat(commandService.getMostProbableCommand("unknown")).isNull();
    }

    @Test
    void getSupportedCommandsHelp() {
        Assertions.assertThat(commandService.getSupportedCommandsHelp())
                .isEqualTo(CommandConfiguration.SUPPORTED_COMMANDS);
    }
}