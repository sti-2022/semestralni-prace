package cz.woi.sti.backend.controller;

import cz.woi.sti.backend.model.ExchangeRate;
import cz.woi.sti.backend.service.ExchangeRatesServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ExchangeRatesControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    ExchangeRatesServiceImpl exchangeRatesService;

    @BeforeEach
    void setUp() {
        Mockito.when(exchangeRatesService.getExchangeRate(any())).thenReturn(0.0);
        Mockito.when(exchangeRatesService.isCurrencyUnsupported("unsupported")).thenCallRealMethod();
        Mockito.when(exchangeRatesService.getRateHistory(any()))
                .thenReturn(List.of(new ExchangeRate(LocalDate.MIN, "EUR", 24.0)));
    }

    @Test
    void testExchangeRateEndpoint_noArg() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/exchange-rate"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }

    @Test
    void testExchangeRateEndpoint_withSupported() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/exchange-rate/EUR"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }

    @Test
    void testExchangeRateEndpoint_withUnsupported() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/exchange-rate/unsupported"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }

    @Test
    void testRateHistoryEndpoint_noArg() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rate-history"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }

    @Test
    void testRateHistoryEndpoint_withSupported() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rate-history/EUR"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }

    @Test
    void testRateHistoryEndpoint_withUnsupported() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/rate-history/unsupported"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotBlank();
    }
}
