package cz.woi.sti.backend.controller;

import cz.woi.sti.backend.service.InformationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class InformationControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    InformationServiceImpl informationService;

    @BeforeEach
    void setUp() {
        Mockito.when(informationService.getApplicationName()).thenReturn("testing");
        Mockito.when(informationService.getCurrentTimezoneTime()).thenReturn("00:00:00");
    }

    @Test
    void testNameEndpoint() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/name"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void testTimeEndpoint() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/time"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
