package cz.woi.sti.backend.controller;

import cz.woi.sti.backend.service.ExchangeDataDownloadServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class InternalControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    ExchangeDataDownloadServiceImpl exchangeDataDownloadService;

    @Test
    void testDownloadDataEndpoint() throws Exception {
        Mockito.doReturn(true).when(exchangeDataDownloadService).downloadSupportedCurrenciesData();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/download-data"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void testDownloadDataEndpoint_failedDownload() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/download-data"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(503));
    }
}
