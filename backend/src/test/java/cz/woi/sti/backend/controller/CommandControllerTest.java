package cz.woi.sti.backend.controller;

import cz.woi.sti.backend.configuration.CommandConfiguration;
import org.apache.commons.lang3.tuple.Triple;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CommandControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void testHelpEndpoint() throws Exception {
        MockHttpServletResponse res = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/v1/help"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(200)).andReturn().getResponse();
        Assertions.assertThat(res.getContentAsString()).isNotEmpty();
    }

    @Test
    void testCommandEndpoint_emptyBody() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/command")
                        .contentType("application/json")
                        .content("{}"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void testCommandEndpoint_unknownCommand() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/command")
                        .contentType("application/json")
                        .content("{\"commandText\":\"foobar\"}"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(422));
    }

    @Test
    void testCommandEndpoint_supportedCommand() throws Exception {
        for (Triple<String, String, String> supportedCommand : CommandConfiguration.SUPPORTED_COMMANDS) {
            MockHttpServletResponse res = mockMvc
                    .perform(MockMvcRequestBuilders.post("/api/v1/command")
                            .content("{\"commandText\":\"" + supportedCommand.getLeft() + "\"}")
                            .contentType("application/json"))
                    .andDo(print())
                    .andExpect(MockMvcResultMatchers.status().is(200)).andReturn().getResponse();
            Assertions.assertThat(res.getContentAsString()).isNotEmpty();
        }
    }
}