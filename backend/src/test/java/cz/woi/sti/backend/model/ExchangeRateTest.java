package cz.woi.sti.backend.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
class ExchangeRateTest {

    ExchangeRate exchangeRate;

    @BeforeEach
    void setUp() {
        exchangeRate = new ExchangeRate(LocalDate.MIN, "MOCK", 1.23);
    }

    @Test
    void getDate() {
        Assertions.assertThat(exchangeRate.getDate()).isEqualTo(LocalDate.MIN);
    }

    @Test
    void getCurrency() {
        Assertions.assertThat(exchangeRate.getCurrency()).isEqualTo("MOCK");
    }

    @Test
    void getRate() {
        Assertions.assertThat(exchangeRate.getRate()).isEqualTo(1.23);
    }

    @Test
    void testEmptyConstructor() {
        new ExchangeRate();
    }

    @Test
    void testPartialConstructor() {
        new ExchangeRate("MOCK", 0.0);
    }

}