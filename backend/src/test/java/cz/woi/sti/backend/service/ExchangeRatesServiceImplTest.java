package cz.woi.sti.backend.service;

import cz.woi.sti.backend.configuration.ExchangeRatesConfiguration;
import cz.woi.sti.backend.model.ExchangeRate;
import cz.woi.sti.backend.repository.ExchangeRateRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ExchangeRatesServiceImplTest {

    @Mock
    ExchangeRateRepository exchangeRateRepository;
    @InjectMocks
    ExchangeRatesServiceImpl exchangeRatesService;

    @BeforeEach
    void setUp() {
        lenient()
                .when(exchangeRateRepository.findByDateAndCurrency(any(), anyString()))
                .thenReturn(new ExchangeRate(LocalDate.now(), "MOCK", 1.23));
        lenient()
                .when(exchangeRateRepository.findAllByCurrency(anyString()))
                .thenReturn(List.of(new ExchangeRate(LocalDate.now(), "MOCK", 1.23)));
    }

    @Test
    void getExchangeRate_supported() {
        for (String supportedCurrency : ExchangeRatesConfiguration.SUPPORTED_CURRENCIES) {
            Assertions.assertThat(exchangeRatesService.getExchangeRate(supportedCurrency)).isNotNull();
        }
    }

    @Test
    void getExchangeRate_unsupported() {
        Throwable e = org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class,
                () -> exchangeRatesService.getExchangeRate("unsupported"));
        Assertions.assertThat("currency not supported").isEqualTo(e.getMessage());
    }

    @Test
    void getRateHistory_supported() {
        for (String supportedCurrency : ExchangeRatesConfiguration.SUPPORTED_CURRENCIES) {
            Assertions.assertThat(exchangeRatesService.getRateHistory(supportedCurrency)).isNotNull();
        }
    }

    @Test
    void getRateHistory_unsupported() {
        Throwable e = org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class,
                () -> exchangeRatesService.getRateHistory("unsupported"));
        Assertions.assertThat("currency not supported").isEqualTo(e.getMessage());
    }

    @Test
    void isCurrencyUnsupported_withSupported() {
        for (String supportedCurrency : ExchangeRatesConfiguration.SUPPORTED_CURRENCIES) {
            Assertions.assertThat(exchangeRatesService.isCurrencyUnsupported(supportedCurrency)).isFalse();
        }
    }

    @Test
    void isCurrencyUnsupported_withUnsupported() {
        Assertions.assertThat(exchangeRatesService.isCurrencyUnsupported("unsupported")).isTrue();
    }
}